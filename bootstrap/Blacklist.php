<?php

namespace app\bootstrap;

use yii\base\BootstrapInterface;

class Blacklist implements BootstrapInterface {

    public function bootstrap($app)
    {

        if(\Yii::$app->request->url == '/black-list') return;

        $ip = \Yii::$app->request->userIP;
        $isBlack = \app\models\Blacklist::find()->where(['ip' => $ip])->count();

        if($isBlack) {
            \Yii::$app->response->redirect('/black-list');
        }
    }

}
