<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name' => 'Zaholod.kz',
    'homeUrl' => '/',
    'language' => 'ru',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'app\widgets\Bootstrap',
        'app\bootstrap\Blacklist'
    ],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'baseUrl' => '/',
            'cookieValidationKey' => 'qKlIsEzK0ZeQuKJfy6vA0eExftv3GoNW',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
                'transport' => [
                    'class' => 'Swift_SmtpTransport',
                    'host' => 'mail.zaholod.kz',
                    'username' => 'robot@zaholod.kz',
                    'password' => 'Tmn?47j6',
                    'port' => '25',
                    'encryption' => false,
                ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'baseUrl' => '/',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'admin' => 'admin/default/index',
                'black-list' => 'site/black-list',
                '<url>' => 'site/page',
                '<controller>/<action>' => '<controller>/<action>',
                '<controller:\w+>'=>'<controller>/index',
               '<_m:[\w\-]+>/<_c:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/view',
               '<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/<_a>',
               '<_m:[\w\-]+>' => '<_m>/default/index',
               '<_m:[\w\-]+>/<_c:[\w\-]+>' => '<_m>/<_c>/index',
               '<_m:[\w\-]+>' => '<_m>/<_c>/index',
            ],
        ],
        'assetManager' => [
	        'converter'=> [
	            'class'=>'nizsheanez\assetConverter\Converter',
	            'force'=>true,
	            'destinationDir' => 'compiled',
	            'parsers' => [
					'less' => [ // file extension to parse
						'class' => 'nizsheanez\assetConverter\Less',
						'output' => 'css', // parsed output file type
						'options' => array(
							'auto' => true, // optional options
						)
					]
				]
	        ]
	    ],
        'reCaptcha' => [
            'class' => 'himiklab\yii2\recaptcha\ReCaptchaConfig',
            'siteKeyV2' => '6LecLrYUAAAAAA9-TUObjP6pDSigMAyh8p1hnb7k',
            'secretV2' => '6LecLrYUAAAAAOl2qIGlTdy7d_kplwTKks891nYC',
            'siteKeyV3' => '6LcOHbYUAAAAALS0_GSyLR5xZ1nZBGAy5MM4kXq-',
            'secretV3' => '6LcOHbYUAAAAAEJZdbSZimJZrZxDCpoy91l78KB4',
        ],
    ],
    'params' => $params,
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
        'redactor' => [
            'class' => 'yii\redactor\RedactorModule',
            'uploadDir' => '@webroot/uploads',
            'uploadUrl' => '@web/uploads',
            'imageAllowExtensions'=>['jpg','png','gif']
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
