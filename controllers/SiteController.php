<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = "index";
        $contact = new ContactForm();

        $txt_content = \app\models\Pages::findOne(['url' => '#']);
        // Yii::$app->session->setFlash('success', 'Ожидайте, мастер свяжется с Вами в ближайшее время.');
        if($contact->load(Yii::$app->request->post())) {
            $status = $contact->contact("info@inparts.kz");

            if($status == true) {
                Yii::$app->session->setFlash('success', 'Ожидайте, мастер свяжется с Вами в ближайшее время.');
            } else {
                Yii::$app->session->setFlash('error', 'Произошла ошибка.');
            }

            return $this->refresh();
        }

        return $this->render('index', [
            'contact' => $contact,
            'txt_content' => $txt_content->text
        ]);
    }

    public function actionBlackList() {
        $this->layout = 'blacklist';
        return $this->render('blacklist');
    }

    public function actionPage($url) {
        $model = \app\models\Pages::findOne(['url'=>$url]);
        if(!$model) throw new \yii\web\NotFoundHttpException('Страница не найдена');

        return $this->render('page', [
            'model' => $model
        ]);
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
