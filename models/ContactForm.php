<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $phone;
    public $text;
    public $reCaptcha;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['text'], 'string', 'max'=>400],
            [['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator2::className(),
                'uncheckedMessage' => 'Подтвердите, что Вы не робот.'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'phone' => 'Номер телефона',
            'text' => 'Сообщение (не обязательно)',
        ];
    }

    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['email_from'])
                ->setTo(Yii::$app->params['email_to'])
                ->setSubject("Посетитель {$this->phone} отправил данные для связи")
                ->setTextBody("Имя: {$this->name} \r\n Телефон: {$this->phone} \r\n Сообщение: {$this->text}")
                ->setHtmlBody("Имя: {$this->name} <br /> Телефон: {$this->phone} <br /> Сообщение: {$this->text}")
                ->send();

            return true;
        }
        return false;
    }
}
