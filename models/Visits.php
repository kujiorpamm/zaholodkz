<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "visits".
 *
 * @property int $id
 * @property string $ip
 * @property int $count
 * @property string $last_date
 */
class Visits extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'visits';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ip', 'count'], 'required'],
            [['count'], 'integer'],
            [['last_date'], 'safe'],
            [['ip'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'IP адрес',
            'count' => 'Количество заходов',
            'last_date' => 'Последняя дата посещения',
        ];
    }

    public function search($params) {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => 100
            ]
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }

        if($this->ip) {
            $query->where("ip like '%{$this->ip}%'");
        }

        return $dataProvider;
    }

}
