<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Blacklist */

$this->title = 'Добавить IP в черный список';
$this->params['breadcrumbs'][] = ['label' => 'Blacklists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blacklist-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
