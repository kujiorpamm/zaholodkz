<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Черный список IP адресов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blacklist-index">

    <p>
        <?= Html::a('Добавить IP в черный список', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'ip',
            'note',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} &nbsp;&nbsp;&nbsp;&nbsp; {delete}'
            ],
        ],
    ]); ?>
</div>
