<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Blacklist */

$this->title = 'Редактировать запись: ' . $model->ip;
$this->params['breadcrumbs'][] = ['label' => 'Blacklists', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="blacklist-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
