<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Меню', 'options' => ['class' => 'header']],
                    ['label' => 'Страницы', 'icon' => 'list-ul', 'url' => ['/admin/pages']],
                    ['label' => 'Посетители', 'icon' => 'list-ul', 'url' => ['/admin/visits']],
                    ['label' => 'Настройки', 'icon' => 'list-ul', 'url' => ['/admin/settings']],
                    ['label' => 'Черный список', 'icon' => 'list-ul', 'url' => ['/admin/black-list']],
                ],
            ]
        ) ?>

    </section>

</aside>
