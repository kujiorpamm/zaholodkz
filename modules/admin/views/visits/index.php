<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Посещения сайта';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="clearfix">
    <div class="pull-right">
        <a class="btn btn-success" href="/admin/visits/delete-all">Удалить все</a>
    </div>
</div>

<div class="visits-index">

    <?= GridView::widget([
        'filterModel' => $model,
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ip',
            ['attribute' => 'count', 'filter' => false],
            ['attribute' => 'last_date', 'filter' => false],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}'
            ],
        ],
    ]); ?>
</div>
