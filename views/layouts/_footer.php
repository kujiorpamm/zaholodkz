<?php

use yii\bootstrap\Modal;

$phone1 = \app\models\Settings::getSettings('phone1');
$_phone1 = str_replace(['(', ')', ' ', '-'], '', $phone1);
$phone2 = \app\models\Settings::getSettings('phone2');
$_phone2 = str_replace(['(', ')', ' ', '-'], '', $phone2);
$whatsapp = \app\models\Settings::getSettings('whatsapp');
$_whatsapp = str_replace(['(', ')', ' ', '-', '+'], '', $whatsapp);
?>

<footer id="footer" class="footer content">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-2">

            </div>
            <div class="col-md-5 col-sm-5">
                <ul class="footer-menu">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/remont-bytovih-holodilnikov">Бытовые холодильники</a></li>
                    <li><a href="/remont-promyshlennyh-holodilnikov-v-almate">Промышленные холодильники</a></li>
                    <li><a href="/remont-torgovyh-holodilnikov-v-almate">Торговые холодильники</a></li>
                </ul>
            </div>
            <div class="col-md-5 col-sm-5">
                <div class="contacts">
                    <span>zaholod.kz</span>
                    <a class="phone" href="tel:<?=$_phone1?>"><?=$phone1?></a> <br />
                    <a class="phone" href="tel:<?=$_phone2?>"><?=$phone2?></a> <span class="name">Александр</span>
                    <span>Республика Казахстан, г.Алматы</span>
                </div>
            </div>
        </div>
    </div>
    <div class="license">
        Разработано : <a href="mailto:kujiorpamm@gmail.com">kujiorpamm@gmail.com</a>
    </div>
</footer>

<?php Modal::begin([
    'id' => 'alert',
    'header' => '<h3>Заявка отправлена!</h3>',
    'clientOptions' => [
        'show' => Yii::$app->session->hasFlash('success')
    ]
    ])
?>

    <p><?=Yii::$app->session->getFlash('success')?></p>

<?php Modal::end(); ?>
