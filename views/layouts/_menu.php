<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
 ?>

<div class="block header">
    <div class="container">
        <!-- DESCTOP -->
        <div class="clearfix header-content hidden-xs">
            <div class="pull-left">
                <div class="logo-container">
                    <div class="logo-image">
                        <a href="/"><img src="/images/logo3.png" /></a>
                    </div>
                    <div class="logo-text">
                        <div class="lt-1">РЕМОНТ ХОЛОДИЛЬНИКОВ В АЛМАТЫ</div>
                        <div class="lt-2">www.zaholod.kz</div>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <div class="address">
                    <div class="address-image">
                        <img src="/images/icon-home.png" />
                    </div>
                    <div class="address-text">
                        <p class="bold">Алматы</p>
                        <p>С 8:00 до 23:00</p>
                        <p>Без выходных!</p>
                    </div>
                </div>
                <div class="addr">

                </div>
            </div>
            <div class="pull-right">
                <div class="phones">
                    <div class="phone-image">
                        <img src="/images/icon-phone.png" />
                    </div>
                    <div class="phone-text">
                        <p> <a href="tel:<?=$_p1?>"><?=$p1?></a> </p>
                        <p> <a href="tel:<?=$_p2?>"><?=$p2?></a> <span>Александр</span></p>
                        <p class="small">
                            или написать в <a href="https://api.whatsapp.com/send?phone=<?=$_wa?>">WhatsApp</a>
                        </p>
                    </div>
                </div>
                <div class="addr">

                </div>
            </div>
        </div>
        <!-- MOBILE -->
        <div class="mobile-menu">
            <div class="clearfix">
                <div class="pull-left">
                    <a href="/"><img src="/images/logo3.png" alt="zaholod.kz"/></a>
                </div>
                <div class="pull-left">
                    <div class="mobile-centered">
                        <div>РЕМОНТ ХОЛОДИЛЬНИКОВ В АЛМАТЫ</div>
                        <div>www.zaholod.kz</div>
                    </div>
                </div>
                <div class="pull-right menu-icon">
                    <button onclick="$('#mobile_menu').modal('show')" class="fas fa-bars"></button>
                </div>
            </div>
        </div>
    </div>
    <div class="menu-row">
        <div class="container">
            <a href="/">Главная</a>
            <a href="/remont-bytovih-holodilnikov">БЫТОВЫЕ ХОЛОДИЛЬНИКИ</a>
            <a href="/remont-torgovyh-holodilnikov-v-almate">ТОРГОВЫЕ ХОЛОДИЛЬНИКИ</a>
            <a href="/remont-promyshlennyh-holodilnikov-v-almate">ПРОМЫШЛЕННЫЕ ХОЛОДИЛЬНИКИ</a>
        </div>
    </div>
</div>

<?php
    Modal::begin([
        'id' => 'mobile_menu',
        'header' => 'zaholod.kz',
        'options' => [
            'class' => 'mobile-menu'
        ],
        'clientOptions' => [
            // 'show' => true
        ]
    ]);
?>

    <ul>
        <li><a href="/">Главная</a></li>
        <li><a href="/remont-bytovih-holodilnikov">БЫТОВЫЕ ХОЛОДИЛЬНИКИ</a></li>
        <li><a href="/remont-torgovyh-holodilnikov-v-almate">ТОРГОВЫЕ ХОЛОДИЛЬНИКИ</a></li>
        <li><a href="/remont-promyshlennyh-holodilnikov-v-almate">ПРОМЫШЛЕННЫЕ ХОЛОДИЛЬНИКИ</a></li>
    </ul>
    <hr />
    <div class="contacts">
    </i> <a href="tel:<?=$_p1?>?>"><?=$p1?></a><br /> <br />
        <a href="tel:<?=$_p2?>"><?=$p2?></a><br /> <br />
        C 8.00 до 23.00<br />Без выходных!
    </div>

<?php Modal::end(); ?>
