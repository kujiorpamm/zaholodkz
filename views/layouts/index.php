<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

$phone1 = \app\models\Settings::getSettings('phone1');
$_phone1 = str_replace(['(', ')', ' ', '-'], '', $phone1);
$phone2 = \app\models\Settings::getSettings('phone2');
$_phone2 = str_replace(['(', ')', ' ', '-'], '', $phone2);
$whatsapp = \app\models\Settings::getSettings('whatsapp');
$_whatsapp = str_replace(['(', ')', ' ', '-', '+'], '', $whatsapp);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="ZjU6OmVaPUaE-jkRvAWe_jnq7df6F08xgmG9z3LTt7M" />
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <?= Html::csrfMetaTags() ?>
    <link href="https://fonts.googleapis.com/css?family=Old+Standard+TT:400i|Playfair+Display:400i&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/solid.css" integrity="sha384-uKQOWcYZKOuKmpYpvT0xCFAs/wE157X5Ua3H5onoRAOCNkJAMX/6QF0iXGGQV9cP" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/fontawesome.css" integrity="sha384-HU5rcgG/yUrsDGWsVACclYdzdCcn5yU8V/3V84zSrPDHwZEdjykadlgI6RHrxGrJ" crossorigin="anonymous">
    <title><?= Html::encode($this->title) ?></title>
    <!-- Global site tag (gtag.js) - Google Ads: 940678320 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-940678320"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-940678320'); </script>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap index">

    <?=$this->render('_menu', [
        'p1' => $phone1,
        '_p1' => $_phone1,
        'p2' => $phone2,
        '_p2' => $_phone2,
        'wa' => $whatsapp,
        '_wa' =>$_whatsapp])?>

    <div class="block banner">
        <div id="dots">
            <div class="container">
                <div class="dots-cont">
                </div>
            </div>
        </div>
        <div class="index-slider">

            <div class="slide-1">
                <div class="container">
                    <div class="slide-content">
                        <div class="row">
                            <div class="related">
                                <!-- <div class="sitename">ZAHOLOD.KZ</div> -->
                                <h1>ПРОФЕССИОНАЛЬНЫЙ РЕМОНТ ХОЛОДИЛЬНИКОВ <br />У ВАС ДОМА</h1>
                                <h2></h2>
                                <div class="catalog-btn">
                                    <button onclick="js:_scrollTo('#app');" class="yellow lg shadow">Вызвать мастера</button>
                                </div>
                                <div class="related-image">
                                    <img src="/images/slide1_new.png" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slide-2">
                <div class="container">
                    <div class="slide-content">
                        <div class="row">
                            <div class="related">
                                <!-- <div class="sitename">ZAHOLOD.KZ</div> -->
                                <h2 class="heading">ПРОФЕССИОНАЛЬНЫЙ РЕМОНТ ПРОМЫШЛЕННЫХ ХОЛОДИЛЬНИКОВ <br />В АЛМАТЫ</h2>
                                <h2></h2>
                                <div class="catalog-btn">
                                    <button onclick="js:_scrollTo('#app');" class="yellow lg shadow">Вызвать мастера </button>
                                </div>
                                <div class="related-image">
                                    <img src="/images/slide3_new.png" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slide-3">
                <div class="container">
                    <div class="slide-content">
                        <div class="row">
                            <div class="related">
                                <!-- <div class="sitename">ZAHOLOD.KZ</div> -->
                                <h2 class="heading">ПРОФЕССИОНАЛЬНЫЙ РЕМОНТ ТОРГОВЫХ ХОЛОДИЛЬНИКОВ<br />В АЛМАТЫ</h2>
                                <h2></h2>
                                <div class="catalog-btn">
                                    <button onclick="js:_scrollTo('#app');" class="yellow lg shadow">Вызвать мастера</button>
                                </div>
                                <div class="related-image">
                                    <img src="/images/slide2_new.png" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <?= $content ?>

</div>

<?=$this->render('_footer')?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
