<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\ContactForm;

AppAsset::register($this);

$phone1 = \app\models\Settings::getSettings('phone1');
$_phone1 = str_replace(['(', ')', ' ', '-'], '', $phone1);
$phone2 = \app\models\Settings::getSettings('phone2');
$_phone2 = str_replace(['(', ')', ' ', '-'], '', $phone2);
$whatsapp = \app\models\Settings::getSettings('whatsapp');
$_whatsapp = str_replace(['(', ')', ' ', '-', '+'], '', $whatsapp);

$contact = new ContactForm();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <meta name="google-site-verification" content="ZjU6OmVaPUaE-jkRvAWe_jnq7df6F08xgmG9z3LTt7M" />
    <?= Html::csrfMetaTags() ?>
<!--    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/solid.css" integrity="sha384-uKQOWcYZKOuKmpYpvT0xCFAs/wE157X5Ua3H5onoRAOCNkJAMX/6QF0iXGGQV9cP" crossorigin="anonymous">-->
<!--    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/fontawesome.css" integrity="sha384-HU5rcgG/yUrsDGWsVACclYdzdCcn5yU8V/3V84zSrPDHwZEdjykadlgI6RHrxGrJ" crossorigin="anonymous">-->
    <title><?= Html::encode($this->title) ?></title>
    <!-- Global site tag (gtag.js) - Google Ads: 940678320 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-940678320"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-940678320'); </script>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap index">

    <?=$this->render('_menu', [
        'p1' => $phone1,
        '_p1' => $_phone1,
        'p2' => $phone2,
        '_p2' => $_phone2,
        'wa' => $whatsapp,
        '_wa' =>$_whatsapp])?>



    <?= $content ?>

    <div class="block application2">
        <div class="container md">
            <div class="row">
                <div class="col-sm-6">
                    <div id="app" class="application-form">
                        <h2>Заявка на ремонт</h2>
                        <?php $form = \yii\widgets\ActiveForm::begin(['action' => '/']) ?>
                        <?=$form->field($contact, 'name')->textInput()->label('Имя')?>
                        <?= $form->field($contact, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                                                'mask' => '+7-999-9999999',
                                                'options' => ['class'=>'form-control js-label']
                                            ])->label('Контактный телефон')?>
                        <?=$form->field($contact, 'text')->textArea()->label('Сообщение')?>
                        <?= $form->field($contact, 'reCaptcha')->widget( \himiklab\yii2\recaptcha\ReCaptcha2::className() )->label(false) ?>
                        <?=\yii\helpers\Html::submitButton('Отправить')?>
                        <?php $form::end()?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-6">
                    <div class="phones">
                        <div class="text-content">
                            <div class="t1">
                                Звоните, буду рад Вам помочь!
                            </div>
                            <div class="t2">
                                <a href="https://api.whatsapp.com/send?phone=<?=$_whatsapp?>"><span>Написать в WhatsApp</span></a>
                            </div>
                        </div>
                        <div class="text-content2">
                            <div class="t1">
                                <i class="glyphicon glyphicon-earphone"></i> &nbsp;&nbsp;&nbsp; <a href="tel:<?=$_phone2?>"> <?=$phone2?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?=$this->render('_footer')?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
