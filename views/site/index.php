<?php

/* @var $this yii\web\View */

$this->title = 'Ремонт холодильников в Алматы. Мастер Александр. Стаж 25 лет! ';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Качественный ремонт холодильников в Алматы всех марок, любой сложности у Вас дома. Гарантия!',
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'ремонт холодильников +на дому, ремонт холодильников +недорого, ремонт холодильников в алматы +цены, частный ремонт холодильника, ремонт холодильника +атлант, ремонт холодильников либхер, мастер +по ремонту холодильников, мастер холодильник, холодильник атлант',
]);

$phone1 = \app\models\Settings::getSettings('phone1');
$_phone1 = str_replace(['(', ')', ' ', '-'], '', $phone1);
$phone2 = \app\models\Settings::getSettings('phone2');
$_phone2 = str_replace(['(', ')', ' ', '-'], '', $phone2);
$whatsapp = \app\models\Settings::getSettings('whatsapp');
$_whatsapp = str_replace(['(', ')', ' ', '-', '+'], '', $whatsapp);


?>


<div class="block about content">
    <div class="container md">


        <!-- <h2>Добрый день</h2>
        <p>У Вас сломался холодильник? Не переживайте!</p>
        <p>• Меня зовут Александр и я имею ОПЫТ РАБОТЫ в сфере ремонта холодильников – более 20 лет!</p>
        <p>• Ремонтирую ВСЕ МАРКИ холодильников, морозильников, будь то бытовой холодильник, морозильник у Вас дома или любое другое холодильное, морозильное оборудование в магазине, кафе, ресторане и т. д.</p>
        <p>• Я отремонтирую Ваш холодильник (устраню неполадки ЛЮБОЙ СЛОЖНОСТИ) - БЫСТРО, КАЧЕСТВЕННО, АККУРАТНО, У ВАС ДОМА и не доставляя Вам излишних хлопот.</p>
        <p>• Гарантия на выполненную работу - 1 год!</p>
        <p>• График работы - БЕЗ ВЫХОДНЫХ.</p>
        <p>• Индивидуальный подход к каждому клиенту - малообеспеченным людям предоставляется СКИДКА!</p>
        <p>• Вызов - диагностика оплачивается в случае отказа от ремонта.</p>
        <p>• Обращаясь ко мне - Вы напрямую сотрудничаете с мастером, БЕЗ ВСЯКИХ ПОСРЕДНИКОВ!</p>
        <p>• Доверьте свой холодильник - ПРОФЕССИОНАЛУ!</p>
        <p>ЗВОНИТЕ или заполняйте заявку на ремонт на сайте - БУДУ РАД ВАМ ПОМОЧЬ. 8(727) 377-91-26, 8(777) 087-60-70</p>
        <p></p> -->
        <?=$txt_content?>
        <button  onclick="js:_scrollTo('#app');">Вызвать мастера </button>

    </div>
</div>

<div class="block phone">
    <div class="container">
        <i class="glyphicon glyphicon-earphone"></i> <a href="tel:<?=$_phone1?>"><?=$phone1?></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <i class="glyphicon glyphicon-earphone hidden-xs"></i>  <a href="tel:<?=$_phone2?>"><?=$phone2?></a>
    </div>
</div>

<div class="block benefits">
    <div class="container md">
        <h2>Обращаясь ко мне - выгода для Вас:</h2>
        <div class="ben-list">
            <div class="ben ben1">
                <div class="heading">МНОГОЛЕТНИЙ ОПЫТ</div>
                <div class="text">Я - мастер с большим опытом и профессиональными навыками.</div>
            </div>
            <div class="ben ben2">
                <div class="heading">ПРОФЕССИОНАЛЬНЫЙ ПОДХОД</div>
                <div class="text">Качественный ремонт холодильников любой сложности у Вас дома.</div>
            </div>
            <div class="ben ben3">
                <div class="heading">УДОБНАЯ СХЕМА РАБОТЫ</div>
                <div class="text">В удобное для Вас время, без выходных и праздников.</div>
            </div>
            <div class="ben ben4">
                <div class="heading">ОПЕРАТИВНОСТЬ</div>
                <div class="text">Минимальный срок выполнения ремонтных работ около одного часа.</div>
            </div>
            <div class="ben ben5">
                <div class="heading">ПРОВЕРЕННЫЕ ПОСТАВЩИКИ</div>
                <div class="text">Я устанавливаю только новые запчасти от проверенных годами производителей.</div>
            </div>
            <div class="ben ben6">
                <div class="heading">ОТКРЫТОЕ ЦЕНООБРАЗОВАНИЕ</div>
                <div class="text">Конечная стоимость за ремонт зависит от сложности поломки и объёма выполненных работ. Цены простые и понятные, без скрытых платежей.</div>
            </div>
            <div class="ben ben7">
                <div class="heading">ГАРАНТИЯ</div>
                <div class="text">Гарантия на выполненную работу - один год!</div>
            </div>
        </div>
    </div>
</div>

<div class="block application2">
    <div class="container md">
        <div class="row">
            <div class="col-sm-6">
                <div id="app" class="application-form">
                    <h2>Заявка на ремонт</h2>
                    <?php $form = \yii\widgets\ActiveForm::begin([
                            'enableAjaxValidation' => false
                    ]) ?>
                    <?=$form->field($contact, 'name')->textInput()->label('Имя')?>
                    <?= $form->field($contact, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                                            'mask' => '+7-999-9999999',
                                            'options' => ['class'=>'form-control js-label']
                                        ])->label('Контактный телефон')?>
                    <?=$form->field($contact, 'text')->textArea()->label('Сообщение')?>
                    <?= $form->field($contact, 'reCaptcha')->widget( \himiklab\yii2\recaptcha\ReCaptcha2::className() )->label(false) ?>
                    <?=\yii\helpers\Html::submitButton('Отправить')?>
                    <?php $form::end()?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-6">
                <div class="phones">
                    <div class="text-content">
                        <div class="t1">
                            Звоните, буду рад Вам помочь!
                        </div>
                        <div class="t2">
                            <a href="https://api.whatsapp.com/send?phone=<?=$_whatsapp?>"><span>Написать в WhatsApp</span></a>
                        </div>
                    </div>
                    <div class="text-content2">
                        <div class="t1">
                            <i class="glyphicon glyphicon-earphone"></i> &nbsp;&nbsp;&nbsp; <a href="tel:<?=$_phone2?>"> <?=$phone2?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
