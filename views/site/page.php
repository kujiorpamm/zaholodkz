<?php

/* @var $this yii\web\View */

$this->title = $model->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Качественный ремонт холодильников в Алматы всех марок, любой сложности у Вас дома. Гарантия!',
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'ремонт холодильников +на дому, ремонт холодильников +недорого, ремонт холодильников в алматы +цены, частный ремонт холодильника, ремонт холодильника +атлант, ремонт холодильников либхер, мастер +по ремонту холодильников, мастер холодильник, холодильник атлант',
]);

?>


<div class="default-page">
    <div class="container md">

        <h1><?= $this->title ?></h1>
        <div class="text">
            <?=$model->text?>
        </div>

    </div>
</div>
