<?php

use yii\widgets\ListView;

?>

<div class="page-catalog">
    <div class="container">
        <h1>Поиск</h1> <hr />
        <p class="description"></p>

        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions' => ['tag' => false],
            'itemView' => '_search_list',
        ]);?>

    </div>
</div>
