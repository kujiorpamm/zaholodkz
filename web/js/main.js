$(document).ready(function() {
    checkAnchor();
})

function checkAnchor() {
    var tag = window.location.hash.substr(1);
    if(tag) {
        console.log(tag);
        $('html, body').animate({
           scrollTop: $('#' + tag).offset().top
       }, 1000);
    } else {
        console.log('-1');
    }
}

function _scrollTo(elem) {
    $('html, body').animate({
       scrollTop: $(elem).offset().top
    }, 1000);
}

document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();
        $('#mobile_menu').modal('hide');
        $('html, body').animate({
           scrollTop: $(this.getAttribute('href')).offset().top
       }, 1000);
    });
});

$(document).ready(function(){
  $('.index-slider').slick({
      speed: 1000,
      autoplaySpeed: 4000,
      autoplay: true,
      pauseOnHover: false,
      fade: true,
      dots: true,
      centerMode: true,
      appendDots: '.dots-cont'
  });
});
