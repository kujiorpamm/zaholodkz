<?php
namespace app\widgets;

use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;
use app\models\Visits;

class Bootstrap implements BootstrapInterface
{

    public function bootstrap($app)
    {
        if(Yii::$app->request->isGet) {
            $ip = $app->request->userIp;
            $model = Visits::findOne(['ip' => $ip]);
            if($model) {
                $model->count = $model->count +1;
            } else {
                $model = new Visits();
                $model->ip = $ip;
                $model->count = 1;
            }
            $model->last_date = date('Y-m-d H:i:s');
            $model->save();
        }

    }
}
